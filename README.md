# craftkfu

> Craftkfu est un outil non-officiel sans aucun lien avec Ankama, éditeur de WAKFU.

Un outil simple, uniquement client-side, aidant pour le crafting sur Wakfu.  

## Utilisation

``` bash
git clone https://gitlab.com/MathiusD/craftkfu

cd craftkfu-src

make
```

## Remerciements

Un grand merci à Isp, Aquatikelfik, Augaroma et Magestik pour leur aide.
