default_target: launch

install:
	@npm install

launch: install
	@npm run dev

build: install
	@npm run build

lint: install
	@npm run lint

format: install
	@npm run format