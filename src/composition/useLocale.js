import { computed } from 'vue'
import { i18n } from '../i18n/locale'

export default () => ({
  lang: computed(() => {
    switch (i18n.global.locale.value) {
      case 'fr':
        return 0
      case 'en':
        return 1
      case 'es':
        return 2
      case 'pt':
        return 3
      default:
        return 1
    }
  }),
})
