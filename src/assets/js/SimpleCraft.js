import { allSeparators, argsForSimpleCraft, defaultValues } from './Parse.js'

class SimpleCraft {
  /**
   * Constructor of Simple Craft
   * @param {String | number | Array<String>} id Id of craft
   * @param {number} qt Qt of Craft
   * @param {boolean} isHidden Craft is Hidden
   * @param {number} recipeSelected recipeSelected
   * @param {Array<SimpleCraft>} craftSpec Spec of components
   * @param {boolean} considerAsIngredient Craft is processed like ingredient
   */
  constructor(
    id,
    qt = defaultValues.quantity,
    isHidden = defaultValues.hidden,
    recipeSelected = defaultValues.recipeSelected,
    craftSpec = defaultValues.craftSpec,
    considerAsIngredient = defaultValues.considerAsIngredient,
  ) {
    this.id = id
    this.qt = qt
    this.isHidden = isHidden
    this.recipeSelected = recipeSelected
    this.craftSpec = craftSpec
    this.considerAsIngredient = considerAsIngredient
  }

  /**
   * For get args of this craft
   * @returns {String} args of this craft
   */
  getArgs() {
    return argsForSimpleCraft(this)
  }

  /**
   * Method for simple craft related to this craft
   * @returns {SimpleCraft} simpleCraft
   */
  getSimpleCraft() {
    return this
  }

  /**
   * Method for get rawId of SimpleCraft
   * @returns rawId
   */
  getRawId() {
    if (typeof this.id == 'string' || typeof this.id == 'number') return String(this.id)
    else {
      let rawId = ''
      this.id.forEach((each) => {
        rawId += (rawId.length == 0 ? '' : allSeparators.type) + each
      })
      return rawId
    }
  }
}

export { SimpleCraft }
