import { allSeparators, typeExpectedShort } from './Parse.js'

const identifiers = {
  provider: '%provider',
  error: '%error',
  card: '%card',
  encodedData: '%encodedData',
}

const typePatternDistant = [
  typeExpectedShort.distant +
    allSeparators.type +
    '{nomDuFournisseurDistant}' +
    allSeparators.type +
    '{codeDeLaRéférenceChezLeFournisseurDistant}',
  typeExpectedShort.distant +
    allSeparators.type +
    '{nameOfDistantProvider}' +
    allSeparators.type +
    '{codeOfRef}',
  typeExpectedShort.distant +
    allSeparators.type +
    '{nombreDelProveedorDistante}' +
    allSeparators.type +
    '{códigoDeLaReferencia}',
  typeExpectedShort.distant +
    allSeparators.type +
    '{nomeDoProvedorDistante}' +
    allSeparators.type +
    '{códigoDeRef}',
]
const typePatternLocal = [
  typeExpectedShort.local + allSeparators.type + '{nomDuPanier}',
  typeExpectedShort.local + allSeparators.type + '{nameOfCard}',
  typeExpectedShort.local + allSeparators.type + '{nombreDeLaTarjeta}',
  typeExpectedShort.local + allSeparators.type + '{nomeDoCartão}',
]
const typePatternIdMinimal = ["{idDel'Objet}", '{idOfObject}', '{idDelObjeto}', '{idDeObjeto}']
const typePatternIdVerbose = [
  'id' + allSeparators.type + typePatternIdMinimal[0],
  'id' + allSeparators.type + typePatternIdMinimal[1],
  'id' + allSeparators.type + typePatternIdMinimal[2],
  'id' + allSeparators.type + typePatternIdMinimal[3],
]

const typePattern = {
  distant: typePatternDistant,
  local: typePatternLocal,
  idMin: typePatternIdMinimal,
  idVerb: typePatternIdVerbose,
}
const lvl = ['Niv.', 'Lv.', 'Niv.', 'Nív.']
const recipe = ['recette', 'recipe', 'receta', 'receita']
const resource = ['ressource', 'resource', 'recurso', 'recurso']
const defaultErrorTitle = [
  'Une erreur est survenue',
  'Error occured',
  'Error ocurrido',
  'Ocorreu um erro',
]
const distantError = [
  'Le fournisseur distant (' +
    identifiers.provider +
    ') a retourné une erreur (' +
    identifiers.error +
    '). Plus de détail peut être trouvé dans la console.',
  'Distant provider (' +
    identifiers.provider +
    ') returning error (' +
    identifiers.error +
    '). More detail may fund in console.',
  'Proveedor remoto (' +
    identifiers.provider +
    ') que devuelve error (' +
    identifiers.error +
    '). Más detalles pueden financiar en la consola.',
  'Erro (' +
    identifiers.error +
    ') de retorno do provedor distante (' +
    identifiers.provider +
    '). Mais detalhes podem ser financiados no console.',
]
const localCardNotFound = [
  'Panier "' + identifiers.card + '" non trouvé',
  'Card "' + identifiers.card + '" not found',
  'Tarjeta "' + identifiers.card + '" no encontrada',
  'Cartão "' + identifiers.card + '" não encontrado',
]
const encodedDataMissing = [
  'Donnée encodée "' + identifiers.encodedData + '" non trouvé',
  'Encoded data "' + identifiers.encodedData + '" not found',
  'Datos codificados "' + identifiers.encodedData + '" no encontrados',
  'Dados codificados "' + identifiers.encodedData + '" não encontrados',
]
const badType = [
  'Mauvais type renseigné',
  'Wrong type provided',
  'Tipo incorrecto proporcionado',
  'Tipo incorreto fornecido',
]
const wrongDistantType = [
  'Une ressource distante doit suivre le format suivant : ' + typePatternDistant[0],
  'A remote resource should be formatted as follows : ' + typePatternDistant[1],
  'Un recurso remoto debe formatearse como sigue : ' + typePatternDistant[2],
  'Um recurso remoto deve ser formatado da seguinte forma : ' + typePatternDistant[3],
]
const wrongLocalType = [
  'Une panier local doit suivre le format suivant : ' + typePatternLocal[0],
  'A local card should follow the following format : ' + typePatternLocal[1],
  'Una tarjeta local debe seguir el siguiente formato : ' + typePatternLocal[2],
  'Uma placa local deve seguir o seguinte formato : ' + typePatternLocal[3],
]
const wrongIdType = [
  "Un identifiant d'objet doit suivre l'un des formats suivants : \"" +
    typePatternIdMinimal[0] +
    '", "' +
    typePatternIdVerbose[0] +
    '"',
  'An object identifier must be in one of the following formats : "' +
    typePatternIdMinimal[1] +
    '", "' +
    typePatternIdVerbose[1] +
    '"',
  'El identificador de objeto deberá estar en uno de los formatos siguientes : "' +
    typePatternIdMinimal[2] +
    '", "' +
    typePatternIdVerbose[2] +
    '"',
  'Um identificador de objeto deve estar em um dos seguintes formatos : "' +
    typePatternIdMinimal[3] +
    '", "' +
    typePatternIdVerbose[3] +
    '"',
]
const typeUnknown = ['Type inconnu', 'Type unknown', 'Tipo desconocido', 'Tipo desconhecido']
const localisationError = [
  'Impossible de trouver la traduction',
  'Unable to found translation',
  'Imposible encontrar la traducción',
  'Não foi possível encontrar a tradução',
]

const texts = {
  common: {
    lvl: lvl,
    recipe: recipe,
    resource: resource,
    localisationError: localisationError,
  },
  notify: {
    errors: {
      defaultTitle: defaultErrorTitle,
      distant: distantError,
      localCardNotFound: localCardNotFound,
      encodedDataMissing: encodedDataMissing,
      wrongType: {
        badType: badType,
        distant: wrongDistantType,
        local: wrongLocalType,
        id: wrongIdType,
        unknown: typeUnknown,
      },
    },
  },
}

const jobs = [
  {
    id: -1,
    name: ['Tous les métiers', 'Every Job', 'Cada oficio', 'Todas as Profissões'],
  },
  { id: 71, name: ['Forestier', 'Lumberjack', 'Leñador', 'Lenhador'] },
  {
    id: 72,
    name: ['Herboriste', 'Herbalist', 'Herbolario', 'Herborista'],
  },
  { id: 73, name: ['Mineur', 'Miner', 'Minero', 'Mineiro'] },
  { id: 64, name: ['Paysan', 'Farmer', 'Campesino', 'Fazendeiro'] },
  { id: 75, name: ['Pêcheur', 'Fisherman', 'Pescador', 'Pescador'] },
  { id: 74, name: ['Trappeur', 'Trapper', 'Peletero', 'Caçador'] },
  { id: 77, name: ['Armurier', 'Armorer', 'Armero', 'Armeiro'] },
  { id: 78, name: ['Bijoutier', 'Jeweler', 'Joyero', 'Joalheiro'] },
  { id: 40, name: ['Boulanger', 'Baker', 'Panadero', 'Padeiro'] },
  { id: 76, name: ['Cuisinier', 'Chef', 'Cocinero', 'Cozinheiro'] },
  { id: 81, name: ['Ebéniste', 'Handyman', 'Ebanista', 'Marceneiro'] },
  {
    id: 83,
    name: ["Maitre d'Armes", 'Weapons Master', 'Maestro de armas', 'Mestre de armas'],
  },
  {
    id: 80,
    name: ['Maroquinier', 'Leather Dealer', 'Marroquiniero', 'Coureiro'],
  },
  { id: 79, name: ['Tailleur', 'Tailor', 'Sastre', 'Alfaiate'] },
]

/**
 * Method for get localised String from data and lang used
 * @param {Array<String>} localisationData Data of this sentence
 * @param {number} langUsed Index of Lang Used
 * @returns localisedString
 */
function getLocalisedString(localisationData, langUsed) {
  return localisationData
    ? localisationData[langUsed]
    : getLocalisedString(localisationError, langUsed)
}

export { identifiers, typePattern, texts, jobs, getLocalisedString }
