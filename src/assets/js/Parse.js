import { SimpleCraft } from './SimpleCraft.js'

const defaultValues = {
  type: 'id',
  quantity: 1,
  hidden: false,
  recipeSelected: 0,
  craftSpec: [],
  considerAsIngredient: false,
}

const typeExpectedShort = {
  id: '',
  distant: 'ref',
  local: 'loc',
  encoded: 'b64',
}

const typeExpected = {
  id: ['identifier', 'id', typeExpectedShort.id],
  distant: [
    'builder',
    'build',
    'reference',
    typeExpectedShort.distant,
    'distant',
    'dist',
    'resource',
    'res',
  ],
  local: ['local', typeExpectedShort.local, 'localStorage', 'navigator', 'nav'],
  encoded: ['encoded', 'base64', typeExpectedShort.encoded],
}

/**
 * For detect type
 * @param {String} typeGiven type given for detection
 * @param {Dict<Array<String>>} typeDict type references
 * @param {defaultType} defaultType type returned by default
 * @returns {String} typeFound
 */
function typeOf(typeGiven, typeDict = typeExpected, defaultType = defaultValues.type) {
  let typeFound = defaultType
  Object.keys(typeDict).forEach((type) => {
    typeDict[type].forEach((expected) => {
      if (expected.toLowerCase() == typeGiven.toLowerCase()) {
        typeFound = type
        return typeFound
      }
    })
  })
  return typeFound
}

const allSeparators = {
  type: ':',
  separator: ',',
  quantity: '-',
  specification: '~',
  specificationSeparator: ';',
  specificationDelimiterStart: '(',
  specificationDelimiterEnd: ')',
  hidden: '!',
  recipeSelected: '?',
  considerAsIngredient: '°',
}

const primarySeparators = {
  separator: allSeparators.separator,
  specification: allSeparators.specification,
  specificationDelimiterStart: allSeparators.specificationDelimiterStart,
  specificationDelimiterEnd: allSeparators.specificationDelimiterEnd,
}

const allSeparatorExceptType = {
  separator: allSeparators.separator,
  quantity: allSeparators.quantity,
  specification: allSeparators.specification,
  specificationSeparator: allSeparators.specificationSeparator,
  specificationDelimiterStart: allSeparators.specificationDelimiterStart,
  specificationDelimiterEnd: allSeparators.specificationDelimiterEnd,
  hidden: allSeparators.hidden,
  recipeSelected: allSeparators.recipeSelected,
  recipeAreComponent: allSeparators.considerAsIngredient,
}

/**
 * For create args for simple craft given
 * @param {Array<SimpleCraft>} crafts Array of Craft used
 * @returns {String} argsRelated
 */
function argsForArrayOfSimpleCraft(crafts) {
  let args = ''
  crafts.forEach((craft) => {
    args = args + (args.length != 0 ? allSeparators.separator : '') + argsForSimpleCraft(craft)
  })
  return args
}

/**
 * For create args for simple craft given
 * @param {SimpleCraft} craft Craft used
 * @returns {String} argsRelated
 */
function argsForSimpleCraft(craft) {
  let args =
    craft.id +
    (craft.isHidden ? allSeparators.hidden : '') +
    (craft.considerAsIngredient ? allSeparators.considerAsIngredient : '') +
    (craft.recipeSelected != 0 ? allSeparators.recipeSelected + craft.recipeSelected : '') +
    (craft.qt > 1 ? allSeparators.quantity + craft.qt : '') +
    (craft.craftSpec.length > 0 ? allSeparators.specification : '')
  craft.craftSpec.forEach((craftSpec) => {
    args =
      args +
      allSeparators.specificationDelimiterStart +
      argsForSimpleCraft(craftSpec) +
      allSeparators.specificationDelimiterEnd
  })
  return args
}

/**
 * For get each entries inside args
 * @param {String} args args given
 * @returns {Array<String>} eachEntries
 */
function extractEachEntries(args) {
  let data = args.split(allSeparators.separator)
  return data
}

/**
 * For know if char is separator
 * @param {String} char char used
 * @param {Dict<String>} separators separators used
 * @returns {boolean} isSeparator
 */
function isSeparator(char, separators = allSeparators) {
  let separatorsList = Object.values(separators)
  let indexOfChar = separatorsList.indexOf(char)
  let separatorFound = indexOfChar != -1
  return separatorFound
}

/**
 * For know if string contain separator
 * @param {String} string
 * @param {Dict<String>} separators
 * @returns {boolean} containSeparator
 */
function containSeparator(string, separators = allSeparators) {
  let separatorFound = false
  for (let char of string) {
    if (!separatorFound) separatorFound = isSeparator(char, separators)
    if (separatorFound) return separatorFound
  }
  return separatorFound
}

/**
 * For get all char before next separator
 * @param {String} arg arg used
 * @param {Dict<String>} separators separators used
 * @returns {String} argUntilNextSeparator
 */
function untilNextSeparator(arg, separators = allSeparators) {
  let out = ''
  if (arg != null)
    for (let char of arg) {
      if (!isSeparator(char, separators)) out = out + char
      else {
        return out
      }
    }
  return out
}

/**
 * For get raw value inside arg
 * @param {String} arg arg given
 * @param {String} separator separator used
 * @param {String} defaultValue default value used
 * @param {Dict<String>} separators separators used
 * @returns {String} rawValueExtracted
 */
function extractRawValue(arg, separator, defaultValue = null, separators = allSeparators) {
  let data = defaultValue
  const split = arg.split(separator)
  if (split.length > 0)
    data = untilNextSeparator(arg.replace(split[0], '').slice(separator.length), separators)
  return data
}

/**
 * For get value inside arg
 * @param {String} arg arg given
 * @param {String} separator separator used
 * @param {String} defaultValue default value used
 * @param {Dict<String>} separators separators used
 * @returns {String} valueExtracted
 */
function extractValue(arg, separator, defaultValue = null, separators = allSeparators) {
  let data = defaultValue
  const split = arg.split(separator)
  if (split.length > 0) data = untilNextSeparator(split[1], separators)
  return data
}

/**
 * For get each value inside arg
 * @param {String} arg arg given
 * @param {String} separator separator used
 * @param {Dict<String>} separators separators used
 * @returns {Array<String>} valueExtracted
 */
function extractEachValue(arg, separator, separators = allSeparators) {
  let data = untilNextSeparator(arg, separators).split(separator)
  return data
}

/**
 * For get string with same qt of firstSeparator and final
 * @param {String} arg arg given
 * @param {String} firstSeparator firstSeparator used
 * @param {String} finalSeparator finalSeparator used
 * @returns {String} extractedString
 */
function extractValueCount(arg, firstSeparator, finalSeparator) {
  let out = ''
  let first = 0
  let final = 0
  for (let char of arg) {
    if (char == firstSeparator) {
      first += 1
    } else if (char == finalSeparator) {
      final += 1
    }
    out = out + char
    if (first == final) {
      return out
    }
  }
  return out
}

/**
 * For get each string with same qt of firstSeparator and final
 * @param {String} arg arg given
 * @param {String} firstSeparator firstSeparator used
 * @param {String} finalSeparator finalSeparator used
 * @returns {Array<String>} eachExtractedString
 */
function extractEachValueCount(arg, firstSeparator, finalSeparator) {
  let argUsed = arg
  let data = []
  while (argUsed.length > 0) {
    let indexArgUsed = argUsed.indexOf(firstSeparator)
    if (indexArgUsed == -1) argUsed = ''
    else {
      argUsed = argUsed.slice(indexArgUsed)
      let rawData = extractValueCount(argUsed, firstSeparator, finalSeparator)
      if (rawData.length == 0) argUsed = argUsed.slice(1)
      else {
        argUsed = argUsed.slice(rawData.length)
        data.push(rawData)
      }
    }
  }
  return data
}

/**
 * For get integer inside arg
 * @param {String} arg arg given
 * @param {String} separator separator used
 * @param {number} defaultValue default value used
 * @param {Dict<String>} separators separators used
 * @returns {number} numberExtracted
 */
function extractInt(arg, separator, defaultValue = 1, separators = allSeparators) {
  let rawData = extractValue(arg, separator, separators)
  let rawInt = Number.parseInt(rawData)
  let data = Number.isInteger(rawInt) ? rawInt : defaultValue
  return data
}

/**
 * For get quantity inside arg
 * @param {String} arg arg given
 * @returns {number} quantityExtracted
 */
function extractQuantity(arg) {
  let rawQuantity = untilNextSeparator(arg, primarySeparators)
  let quantity = extractInt(rawQuantity, allSeparators.quantity, defaultValues.quantity)
  return quantity
}

/**
 * For get hidden inside arg
 * @param {String} arg arg given
 * @returns {boolean} isHidden
 */
function extractHidden(arg) {
  let rawHidden = untilNextSeparator(arg, primarySeparators)
  let isHidden =
    rawHidden.indexOf(allSeparators.hidden) != -1 ? !defaultValues.hidden : defaultValues.hidden
  return isHidden
}

/**
 * For get considerAsIngredient inside arg
 * @param {String} arg arg given
 * @returns {boolean} considerAsIngredient
 */
function extractConsiderAsIngredient(arg) {
  let rawConsiderAsIngredient = untilNextSeparator(arg, primarySeparators)
  let considerAsIngredient =
    rawConsiderAsIngredient.indexOf(allSeparators.considerAsIngredient) != -1
      ? !defaultValues.considerAsIngredient
      : defaultValues.considerAsIngredient
  return considerAsIngredient
}

/**
 * For get recipeSelected inside arg
 * @param {String} arg arg given
 * @returns {number} recipeSelected
 */
function extractRecipeSelected(arg) {
  let rawRecipeSelected = untilNextSeparator(arg, primarySeparators)
  let recipeSelected = extractInt(
    rawRecipeSelected,
    allSeparators.recipeSelected,
    defaultValues.recipeSelected,
  )
  return recipeSelected
}

/**
 * For get craftSpec inside arg
 * @param {*} arg arg used
 * @return {Array<String>} craftSpecs
 */
function extractEachCraftSpec(arg) {
  let specAll = extractRawValue(arg, allSeparators.specification, '', {})
  let specSplitted = extractEachValueCount(
    specAll,
    allSeparators.specificationDelimiterStart,
    allSeparators.specificationDelimiterEnd,
  )
  let specRefined = []
  specSplitted.forEach((initialData) => {
    let startSpectDelimPosition = initialData.slice(
      0,
      allSeparators.specificationDelimiterStart.length,
    )
    let rawSpecData =
      startSpectDelimPosition == allSeparators.specificationDelimiterStart
        ? initialData.slice(allSeparators.specificationDelimiterStart.length)
        : initialData
    let endSpecDelimPosition = rawSpecData.slice(
      rawSpecData.length - allSeparators.specificationDelimiterEnd.length,
    )
    rawSpecData =
      endSpecDelimPosition == allSeparators.specificationDelimiterEnd
        ? rawSpecData.slice(0, rawSpecData.length - allSeparators.specificationDelimiterEnd.length)
        : rawSpecData
    specRefined.push(rawSpecData)
  })
  return specRefined
}

/**
 * For get type inside arg
 * @param {String} arg arg used ²
 * @returns {Array<String>} typeArray
 */
function extractType(arg) {
  let rawType = untilNextSeparator(arg, allSeparatorExceptType)
  let type = extractEachValue(rawType, allSeparators.type, allSeparatorExceptType)
  return type
}

/**
 * For get simpleCraft
 * @param {String} arg arg used
 * @returns {SimpleCraft} simpleCraftExtracted
 */
function extractArg(arg) {
  let data = null
  if (arg.length > 0) {
    let specs = []
    extractEachCraftSpec(arg).forEach((spec) => {
      specs.push(extractArg(spec))
    })
    data = new SimpleCraft(
      extractType(arg),
      extractQuantity(arg),
      extractHidden(arg),
      extractRecipeSelected(arg),
      specs,
      extractConsiderAsIngredient(arg),
    )
  }
  return data
}

/**
 * For get all simpleCraft
 * @param {String} args args used
 * @returns {Array<SimpleCraft>} allSimpleCraftExtracted
 */
function extractEachArg(args) {
  let crafts = []
  if (args.length > 0) {
    extractEachEntries(args).forEach((craft) => {
      let data = extractArg(craft)
      if (data != null) crafts.push(data)
    })
  }
  return crafts
}

export {
  defaultValues,
  typeExpectedShort,
  typeExpected,
  typeOf,
  allSeparators,
  primarySeparators,
  allSeparatorExceptType,
  argsForSimpleCraft,
  argsForArrayOfSimpleCraft,
  extractEachEntries,
  isSeparator,
  containSeparator,
  untilNextSeparator,
  extractRawValue,
  extractValueCount,
  extractEachValueCount,
  extractValue,
  extractEachValue,
  extractInt,
  extractQuantity,
  extractHidden,
  extractRecipeSelected,
  extractEachCraftSpec,
  extractType,
  extractArg,
  extractEachArg,
}
