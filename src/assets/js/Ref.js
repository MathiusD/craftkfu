/* eslint-disable-next-line no-unused-vars */
import { Craft, CraftData } from './Craft.js'
/* eslint-disable-next-line no-unused-vars */
import { SimpleCraft } from './SimpleCraft.js'

class Ref {
  /**
   * Constructor of Ref
   * @param {string} name Name related to this ref
   * @param {string} code Code related to this ref
   * @param {URL} link Link of this ref
   * @param {URL} icon Icon used for this ref
   * @param {number} iteration Iteration of this ref
   * @param {Array<SimpleCraft>} recipes recipes required by this ref
   * @param {Array<SimpleCraft>} craftSpec crafts specification related to this ref
   * @param {boolean} isHidden Craft is Hidden
   * @param {number} recipeSelected recipeSelected
   * @param {boolean} considerAsIngredient Craft is processed like ingredient
   * @param {CraftData} craftData Craft data of this ref
   * @param {Craft} currentCraft Current craft represented
   */
  constructor(
    name,
    code,
    link,
    icon,
    iteration,
    recipes,
    craftSpec,
    isHidden,
    recipeSelected,
    considerAsIngredient,
    craftData = null,
    currentCraft = null,
  ) {
    this.name = name
    this.code = code
    this.link = link
    this.icon = icon
    this.iteration = iteration
    this.recipes = recipes
    this.isHidden = isHidden
    this.recipeSelected = recipeSelected
    this.craftSpec = craftSpec
    this.craftData = craftData
    this.currentCraft = currentCraft
  }
}

export { Ref }
