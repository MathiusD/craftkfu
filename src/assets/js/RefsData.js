import { SimpleCraft } from './SimpleCraft.js'

/**
 * Refs defined and used by Craftkfu
 */
const refs = {
  zenith: {
    /**
     * Icon used by this refs
     */
    icon: 'https://zenithwakfu.com/images/header/zenith-logo.webp',
    /**
     * Method for get api endpoint
     * @param {String} code code of ref
     * @returns {URL} apiEndpoint
     */
    apiEndpoint(code) {
      return 'https://api.zenithwakfu.com/api/build/data/' + code
    },
    /**
     * Method for get request of api endpoint
     * @param {String} _code code of ref
     * @returns {RequestInit}
     */
    /* eslint-disable-next-line no-unused-vars */
    apiRequest(_code) {
      return {
        method: 'GET',
      }
    },
    /**
     * Method for get link of ref
     * @param {*} code code of ref
     * @returns {URL} urlOfRef
     */
    link(code) {
      return 'https://zenithwakfu.com/builder/' + code
    },
    /**
     * Method for extract Data from api response
     * @param {Dict} json data send by ref
     * @returns {Array<SimpleCraft>} ItemRelatedToThisRef
     */
    extract(json) {
      let recipes = []
      if (!('items' in json)) throw new Error('Data is missing in response')
      json.items.forEach((item) => {
        if ('id' in item) recipes.push(new SimpleCraft(item.id))
      })
      return recipes
    },
  },
  slummp: {
    /**
     * Icon used by this refs
     */
    icon: 'http://slummp.ddns.net/wakfubuilder/images/favicon.png',
    /**
     * Method for get api endpoint
     * @param {String} _code code of ref
     * @returns {URL} apiEndpoint
     */
    /* eslint-disable-next-line no-unused-vars */
    apiEndpoint(_code) {
      return (
        'https://corsproxy.io/?' +
        encodeURIComponent('http://slummp.ddns.net/wakfubuilder/load.php')
      )
    },
    /**
     * Method for get request of api endpoint
     * @param {String} code code of ref
     * @returns {RequestInit}
     */
    apiRequest(code) {
      let data = new FormData()
      data.append('keys', true)
      data.append('id', code)
      return {
        method: 'POST',
        body: data,
      }
    },
    /**
     * Method for get link of ref
     * @param {*} code code of ref
     * @returns {URL} urlOfRef
     */
    link(code) {
      return 'http://slummp.ddns.net/wakfubuilder/?build=' + code
    },
    /**
     * Method for extract Data from api response
     * @param {Dict} json data send by ref
     * @returns {Array<SimpleCraft>} ItemRelatedToThisRef
     */
    extract(json) {
      let recipes = []
      if (!('items' in json)) throw new Error('Data is missing in response')
      json.items.forEach((item) => {
        if ('id' in item) recipes.push(new SimpleCraft(item.id))
      })
      return recipes
    },
  },
}

export { refs }
