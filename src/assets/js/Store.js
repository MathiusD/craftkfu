import { DataStorage } from './DataStorage.js'
import { getLocalisedString } from './Localisation.js'
import { data } from './Data.js'

class Store {
  /**
   * Constructor of Store
   * @param {number} lang Index of lang used
   * @param {boolean} argsProvided Flag for specify if args are given
   * @param {DataStorage} dataStorage DataStorage related to this Store
   * @param {Array<Dict>} gameData data of game used
   * @param {boolean} autoSavedRestored Flag for specify if autoSave are restored
   */
  constructor(
    lang = 1,
    argsProvided = false,
    dataStorage = new DataStorage(),
    gameData = data,
    autoSavedRestored = false,
  ) {
    this.lang = lang
    this.argsProvided = argsProvided
    this.dataStorage = dataStorage
    this.dataStorage.setStoreRelated(this)
    this.gameData = gameData
    this.autoSavedRestored = autoSavedRestored
  }

  /**
   * Method for get localised String from data and lang defined inside Store
   * @param {Array<String>} localisationData Data of this sentence
   * @returns localisedString
   */
  getLocalisedString(localisationData) {
    return getLocalisedString(localisationData, this.lang)
  }

  /**
   * For fetch recipes from uri given
   * @param {String} uri Uri used
   * @param {Boolean} eraseCurrentData Flag for specify if current dataStorage are erased
   */
  fetchRecipes(uri, eraseCurrentData) {
    const query = decodeURI(uri.substring(1))
    if (eraseCurrentData != null && eraseCurrentData) this.dataStorage.clear()
    this.argsProvided = uri.length >= 2
    this.dataStorage.fetchRecipes(query)
  }
}

let store = new Store()

export { Store, store }
