import { Craft } from './Craft.js'
/* eslint-disable-next-line no-unused-vars */
import { SimpleCraft } from './SimpleCraft.js'
/* eslint-disable-next-line no-unused-vars */
import { CraftData } from './Craft.js'
/* eslint-disable-next-line no-unused-vars */
import { IngredientData } from './Ingredient.js'
import { Ingredient } from './Ingredient.js'
import { defaultValues } from './Parse.js'
import { cardPrefix, saveCard } from './Card.js'

class List {
  /**
   * Constructor of list
   * @param {Array<Craft|Ingredient>} craft crafts register inside this list
   */
  constructor(craft = []) {
    this.crafts = craft
    this.crafts.forEach((item) => item.registerOnChange(this._internalOnChange()))
    this._onChange = () => {}
  }

  /**
   * For get trigger on change if item are changed
   * @param {List} list list handle call
   * @returns {(item: Craft|Ingredient) => void} functionInternal
   */
  _internalOnChange(list = this) {
    return (item) => {
      if (item == list) throw Error('Recursive call !')
      list._onChange(list)
    }
  }

  /**
   * For register method trigerred on each change
   * @param {(ingredient: List) => void} listener
   */
  registerOnChange(listener) {
    this._onChange = listener
  }

  /**
   * Method return condition for found specific item
   * @param {CraftData|IngredientData} craft item research
   * @returns {Function} conditionForThisCraft
   */
  _condFind(craft) {
    return (x) => x.id === craft.id
  }

  /**
   * Method for found specific index of craft
   * @param {CraftData|IngredientData} craft
   * @returns {number} indexOfCraft
   */
  _findIndex(craft) {
    return this.crafts.findIndex(this._condFind(craft))
  }

  /**
   * Method for found specific index of craft
   * @param {CraftData|IngredientData} craft
   * @returns {Craft|Ingredient} craftFounded
   */
  _find(craft) {
    return this.crafts.find(this._condFind(craft))
  }

  /**
   * Method for add craft inside list
   * @param {CraftData|IngredientData} craft craft used for addition
   * @param {number} qt qt used for addition
   * @param {Array<SimpleCraft>} craftSpec crafts specification related to this craft
   * @param {boolean} isHiding if this craft is hiding
   * @param {number} recipeSelected recipe selected for this craft
   * @param {boolean} considerAsIngredient Craft is processed like ingredient
   */
  add(
    craft,
    qt = defaultValues.quantity,
    craftSpec = defaultValues.craftSpec,
    isHiding = defaultValues.hidden,
    recipeSelected = defaultValues.recipeSelected,
    considerAsIngredient = defaultValues.considerAsIngredient,
  ) {
    let index = this._findIndex(craft)
    if (index == -1) {
      this.crafts.push(
        craft.ingredients != null
          ? new Craft(craft, 1, craftSpec, isHiding, recipeSelected, considerAsIngredient)
          : new Ingredient(craft, 1, 0, isHiding),
      )
      qt -= 1
      index = this._findIndex(craft)
    }
    this.crafts[index].registerOnChange(() => {})
    this.crafts[index].changeQt(parseInt(this.crafts[index].qt) + parseInt(qt))
    this.crafts[index].registerOnChange(this._internalOnChange())
    this._onChange(this)
  }

  /**
   * Method for remove craft inside list
   * @param {CraftData|IngredientData} craft craft used for deletion
   * @param {number} qt qt wanted to delete (null is used for all)
   */
  remove(c, qt = null) {
    let index = this._findIndex(c)
    if (index != -1) {
      if (qt != null && this.crafts[index].qt > qt) {
        this.crafts[index].registerOnChange(() => {})
        this.crafts[index].changeQt(this.crafts[index].qt - qt)
        this.crafts[index].registerOnChange(this._internalOnChange())
      } else this.crafts.splice(index, 1)
      this._onChange(this)
    }
  }

  /**
   * Method for get all simple craft related to this list
   * @returns {Array<SimpleCraft>} allSimpleCraft
   */
  getAllSimpleCraft() {
    let out = []
    this.crafts.forEach((craft) => {
      out.push(craft.getSimpleCraft())
    })
    return out
  }

  /**
   * Method for get quantity of specific craft
   * @param {Craft|Ingredient} craft craft used for get his quantity
   * @returns {number} qtRelatedToThisCraft
   */
  quantityOf(craft) {
    let craftFounded = this._find(craft)
    return craftFounded != null ? craftFounded.qt : 0
  }

  /**
   * Method for reset this list
   */
  clear() {
    this.crafts = []
    this._onChange(this)
  }

  /**
   * For save card
   * @param {String} name name of card
   * @param {String} prefix prefix for card
   * @returns {Error} errorIfOccured
   */
  save(name, prefix = cardPrefix) {
    let errorCatched = null
    try {
      saveCard(name, this, prefix)
    } catch (error) {
      console.error(
        'Error occured on save probably due to QuotaExceededError (Error : ',
        error,
        ')',
      )
      errorCatched = error
    }
    return errorCatched
  }

  /**
   * Apply callback in all crafts in this list
   * @param {*} callbackfn callback applyed in crafts
   */
  forEach(callbackfn) {
    this.crafts.forEach(callbackfn)
  }
}

export { List }
