import { cardPrefix } from './Card.js'

class SaveInstruction {
  /**
   * Construction of SaveInstruction
   * @param {String} name name of card
   * @param {boolean} autoSave if autoSave is enable
   * @param {boolean} hideLog if log are hidden
   * @param {String} prefix prefix for card
   */
  constructor(name, autoSave = false, hideLog = false, prefix = cardPrefix) {
    this.name = name
    this.autoSave = autoSave
    this.hideLog = hideLog
    this.prefix = prefix
  }
}

export { SaveInstruction }
