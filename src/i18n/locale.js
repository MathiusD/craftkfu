import { createI18n } from 'vue-i18n'
import en from './en.json'
import es from './es.json'
import fr from './fr.json'
import pt from './pt.json'

export const i18n = createI18n({
  legacy: false,
  globalInjection: true,
  locale: 'fr',
  messages: { fr, en, es, pt },
})
